import React from 'react'


const Table = () => {

    return(
        <>
        <div className="panel-separator"></div>
                <div className="panel-pane pane-block pane-views-real-estate-block">

                  <section id="recent-properties-slider" className="wrap recent-properties recent-properties-slider">
                    <div className="container">
                      <div className="recent-properties-inner">
                        <div className="section-title">
                          <h3>Recent Properties</h3>
                        </div>
                        <div className="recent-properties-content">
                          <div className="caroufredsel-wrap">
                            <ul>
                              <li>
                                <div className="property-row">
                                  <article className="hentry has-featured">
                                    <div className="property-featured">
                                      <a className="content-thumb" href="/citilights/property/southwest-39th-terrace">
                                        <img
                                          src={require('../../images/property1.jpg')}
                                          className="attachment-property-thumb" alt="" />
                                      </a>
                                      <span className="property-category">
                                        <a href="/citilights/taxonomy/term/12" typeof="skos:Concept"
                                          property="rdfs:label skos:prefLabel" datatype="">Co-op</a> </span>
                                      <div className="property-detail">
                                        <div className="size"><span>500 sqft</span>
                                        </div>
                                        <div className="bathrooms"><span>3</span>
                                        </div>
                                        <div className="bedrooms"><span>3</span>
                                        </div>
                                      </div>
                                    </div>
                                    <div className="property-wrap">
                                      <h2 className="property-title"><a href="/"
                                          title="Southwest 39th Terrace">Southwest 39th Terrace</a></h2>
                                      <div className="property-excerpt">
                                        <p>Suspendisse blandit ligula turpis, ac convallis risus fermentum non. Duis
                                          vestibulum quis quam vel...</p>
                                      </div>
                                    </div>
                                    <div className="property-summary">
                                      <div className="property-info">
                                        <div className="property-price">
                                          <span>
                                            <span className="amount">
                                              $ 895,000 </span>
                                          </span>
                                        </div>
                                        <div className="property-action">
                                          <a href="/">More Details</a>
                                        </div>
                                      </div>
                                    </div>
                                  </article>
                                  <article className="hentry has-featured">
                                    <div className="property-featured">
                                      <a className="content-thumb" href="/">
                                        <img
                                          src={require('../../images/property2.jpg')}
                                          className="attachment-property-thumb" alt="" />
                                      </a>
                                      <span className="property-category">
                                        <a href="/" typeof="skos:Concept"
                                          property="rdfs:label skos:prefLabel" datatype="">Apartment</a> </span>
                                      <div className="property-detail">
                                        <div className="size"><span>200 sqft</span>
                                        </div>
                                        <div className="bathrooms"><span>3</span>
                                        </div>
                                        <div className="bedrooms"><span>3</span>
                                        </div>
                                      </div>
                                    </div>
                                    <div className="property-wrap">
                                      <h2 className="property-title"><a href="/"
                                          title="Avenida De La">Avenida De La</a></h2>
                                      <div className="property-excerpt">
                                        <p>Suspendisse blandit ligula turpis, ac convallis risus fermentum non. Duis
                                          vestibulum quis quam vel...</p>
                                      </div>
                                    </div>
                                    <div className="property-summary">
                                      <div className="property-info">
                                        <div className="property-price">
                                          <span>
                                            <span className="amount">
                                              $ 200,000 </span>
                                          </span>
                                        </div>
                                        <div className="property-action">
                                          <a href="/">More Details</a>
                                        </div>
                                      </div>
                                    </div>
                                  </article>
                                </div>
                                <div className="property-row">
                                  <article className="hentry has-featured">
                                    <div className="property-featured">
                                      <a className="content-thumb" href="/">
                                        <img
                                          src={require('../../images/property4.jpg')}
                                          className="attachment-property-thumb" alt="" />
                                      </a>
                                      <span className="property-category">
                                        <a href="/" typeof="skos:Concept"
                                          property="rdfs:label skos:prefLabel" datatype="">Single Family Home</a>
                                      </span>
                                      <div className="property-detail">
                                        <div className="size"><span>180 sqft</span>
                                        </div>
                                        <div className="bathrooms"><span>2</span>
                                        </div>
                                        <div className="bedrooms"><span>3</span>
                                        </div>
                                      </div>
                                    </div>
                                    <div className="property-wrap">
                                      <h2 className="property-title"><a href="/"
                                          title="Precy Villa Condos">Precy Villa Condos</a></h2>
                                      <div className="property-excerpt">
                                        <p>Suspendisse blandit ligula turpis, ac convallis risus fermentum non. Duis
                                          vestibulum quis quam vel...</p>
                                      </div>
                                    </div>
                                    <div className="property-summary">
                                      <div className="property-info">
                                        <div className="property-price">
                                          <span>
                                            <span className="amount">
                                              $ 500,000 </span>
                                          </span>
                                        </div>
                                        <div className="property-action">
                                          <a href="/">More Details</a>
                                        </div>
                                      </div>
                                    </div>
                                  </article>
                                  <article className="hentry has-featured">
                                    <div className="property-featured">
                                      <a className="content-thumb" href="/">
                                        <img
                                          src={require('../../images/property5.jpg')}
                                          className="attachment-property-thumb" alt="" />
                                      </a>
                                      <span className="property-category">
                                        <a href="/" typeof="skos:Concept"
                                          property="rdfs:label skos:prefLabel" datatype="">Condo</a> </span>
                                      <div className="property-detail">
                                        <div className="size"><span>1,913 sqft</span>
                                        </div>
                                        <div className="bathrooms"><span>3</span>
                                        </div>
                                        <div className="bedrooms"><span>4</span>
                                        </div>
                                      </div>
                                    </div>
                                    <div className="property-wrap">
                                      <h2 className="property-title"><a href="/"
                                          title="Visalia, NJ 93277">Visalia, NJ 93277</a></h2>
                                      <div className="property-excerpt">
                                        <p>This 4 bedroom home sits on an oversized lot at the end of a cul-de-sac in an
                                          established...</p>
                                      </div>
                                    </div>
                                    <div className="property-summary">
                                      <div className="property-info">
                                        <div className="property-price">
                                          <span>
                                            <span className="amount">
                                              $ 154,500 </span>
                                          </span>
                                        </div>
                                        <div className="property-action">
                                          <a href="/">More Details</a>
                                        </div>
                                      </div>
                                    </div>
                                  </article>
                                </div>
                              </li>
                              <li>
                                <div className="property-row">
                                  <article className="hentry has-featured">
                                    <div className="property-featured">
                                      <a className="content-thumb" href="/">
                                        <img
                                          src={require('../../images/property6.jpg')}
                                          className="attachment-property-thumb" alt="" />
                                      </a>
                                      <span className="property-category">
                                        <a href="/" typeof="skos:Concept"
                                          property="rdfs:label skos:prefLabel" datatype="">Apartment</a> </span>
                                      <div className="property-detail">
                                        <div className="size"><span>762 sqft</span>
                                        </div>
                                        <div className="bathrooms"><span>3</span>
                                        </div>
                                        <div className="bedrooms"><span>4</span>
                                        </div>
                                      </div>
                                    </div>
                                    <div className="property-wrap">
                                      <h2 className="property-title"><a href="/"
                                          title="The Helux">The Helux</a></h2>
                                      <div className="property-excerpt">
                                        <p>Located on 43rd Street between 10th and 11th Avenue in the hot Midtown West
                                          neighborhood of Hell?s...</p>
                                      </div>
                                    </div>
                                    <div className="property-summary">
                                      <div className="property-info">
                                        <div className="property-price">
                                          <span>
                                            <span className="amount">
                                              $ 3,515 </span>
                                          </span>
                                        </div>
                                        <div className="property-action">
                                          <a href="/">More Details</a>
                                        </div>
                                      </div>
                                    </div>
                                  </article>
                                  <article className="hentry has-featured">
                                    <div className="property-featured">
                                      <a className="content-thumb" href="/">
                                        <img
                                         src={require('../../images/property7.jpg')}
                                          className="attachment-property-thumb" alt="" />
                                      </a>
                                      <span className="property-category">
                                        <a href="/" typeof="skos:Concept"
                                          property="rdfs:label skos:prefLabel" datatype="">Single Family Home</a>
                                      </span>
                                      <div className="property-detail">
                                        <div className="size"><span>1,118 sqft</span>
                                        </div>
                                        <div className="bathrooms"><span>5</span>
                                        </div>
                                        <div className="bedrooms"><span>5</span>
                                        </div>
                                      </div>
                                    </div>
                                    <div className="property-wrap">
                                      <h2 className="property-title"><a
                                          href="/"
                                          title="Single Family Residential, NJ">Single Family Residential, NJ</a></h2>
                                      <div className="property-excerpt">
                                        <p>classNameic 60's ranch living. House has hardwood floors and hard coat plaster
                                          walls and ceilings in...</p>
                                      </div>
                                    </div>
                                    <div className="property-summary">
                                      <div className="property-info">
                                        <div className="property-price">
                                          <span>
                                            <span className="amount">
                                              $ 299,000 </span>
                                          </span>
                                        </div>
                                        <div className="property-action">
                                          <a href="/">More Details</a>
                                        </div>
                                      </div>
                                    </div>
                                  </article>
                                </div>
                                <div className="property-row">
                                  <article className="hentry has-featured">
                                    <div className="property-featured">
                                      <a className="content-thumb" href="/">
                                        <img
                                          src={require('../../images/property8.jpg')}
                                          className="attachment-property-thumb" alt="" />
                                      </a>
                                      <span className="property-category">
                                        <a href="/" typeof="skos:Concept"
                                          property="rdfs:label skos:prefLabel" datatype="">Single Family Home</a>
                                      </span>
                                      <div className="property-detail">
                                        <div className="size"><span>1,304 sqft</span>
                                        </div>
                                        <div className="bathrooms"><span>2</span>
                                        </div>
                                        <div className="bedrooms"><span>3</span>
                                        </div>
                                      </div>
                                    </div>
                                    <div className="property-wrap">
                                      <h2 className="property-title"><a href="/"
                                          title="Peter Cooper Village">Peter Cooper Village</a></h2>
                                      <div className="property-excerpt">
                                        <p>Peter Cooper Village/ Stuyvesant Town provides an unbeatable combination of
                                          city energy and...</p>
                                      </div>
                                    </div>
                                    <div className="property-summary">
                                      <div className="property-info">
                                        <div className="property-price">
                                          <span>
                                            <span className="amount">
                                              $ 5,109 </span>
                                          </span>
                                        </div>
                                        <div className="property-action">
                                          <a href="/">More Details</a>
                                        </div>
                                      </div>
                                    </div>
                                  </article>
                                  <article className="hentry has-featured">
                                    <div className="property-featured">
                                      <a className="content-thumb" href="/">
                                        <img
                                          src={require('../../images/property9.jpg')}
                                          className="attachment-property-thumb" alt="" />
                                      </a>
                                      <span className="property-category">
                                        <a href="/" typeof="skos:Concept"
                                          property="rdfs:label skos:prefLabel" datatype="">Condo</a> </span>
                                      <div className="property-detail">
                                        <div className="size"><span>1,856 sqft</span>
                                        </div>
                                        <div className="bathrooms"><span>2</span>
                                        </div>
                                        <div className="bedrooms"><span>2</span>
                                        </div>
                                      </div>
                                    </div>
                                    <div className="property-wrap">
                                      <h2 className="property-title"><a href="/"
                                          title="Ocala, FL34481">Ocala, FL34481</a></h2>
                                      <div className="property-excerpt">
                                        <p>Wonderful expanded end-unit Augusta featuring 2 bedrooms in split-bedroom
                                          plan, study/den/library...</p>
                                      </div>
                                    </div>
                                    <div className="property-summary">
                                      <div className="property-info">
                                        <div className="property-price">
                                          <span>
                                            <span className="amount">
                                              $ 95,000 </span>
                                          </span>
                                        </div>
                                        <div className="property-action">
                                          <a href="/">More Details</a>
                                        </div>
                                      </div>
                                    </div>
                                  </article>
                                </div>
                              </li>
                            </ul>
                          </div>
                          <a className="caroufredsel-prev" href="#"></a>
                          <a className="caroufredsel-next" href="#"></a>
                        </div>
                      </div>
                    </div>
                  </section>


                </div>
                </>
    );

}

export default Table;
