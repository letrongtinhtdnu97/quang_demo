import React from 'react'


const Feature = () => {

    return(
        <>
        
        <div className="panel-separator"></div>
                <div className="panel-pane pane-block pane-views-real-estate-block-1">

                  <section id="recent-properties-featured" className="wrap recent-properties recent-properties-featured">
                    <div className="container">
                      <div className="recent-properties-inner">
                        <div className="recent-properties-title">
                          <h3>Featured Properties</h3>
                        </div>
                        <div className="recent-properties-content">
                          <div className="caroufredsel-wrap">
                            <ul>
                              <li>
                                <article className="hentry has-featured">
                                  <div className="property-featured">
                                    <a className="content-thumb" href="/">
                                      <img
                                        src={require('../../images/featured-properties1.jpg')}
                                        className="attachment-property-image" alt="" /> </a>
                                    <span className="property-category"><a href="/"
                                        typeof="skos:Concept" property="rdfs:label skos:prefLabel"
                                        datatype="">Apartment</a></span>
                                  </div>
                                  <div className="property-wrap">
                                    <h2 className="property-title"><a href="/"
                                        title="Visalia, NJ 93277">Visalia, NJ 93277</a></h2>
                                    <div className="property-excerpt">
                                      <p>This 4 bedroom home sits on an oversized lot at the end of a cul-de-sac in an
                                        established neighborhood. It is in need of work however would make a great...
                                      </p>
                                    </div>
                                    <div className="property-summary">
                                      <div className="property-detail">
                                        <div className="size"><span>1,913 sqft</span>
                                        </div>
                                        <div className="bathrooms"><span>2</span>
                                        </div>
                                        <div className="bedrooms"><span>3</span>
                                        </div>
                                      </div>
                                      <div className="property-info">
                                        <div className="property-price">
                                          <span>
                                            <span className="amount">
                                              $ 154,500 </span>
                                          </span>
                                        </div>
                                        <div className="property-action">
                                          <a href="/">More Details <i
                                              className="fa fa-arrow-circle-o-right"></i></a>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </article>
                              </li>
                              <li>
                                <article className="hentry has-featured">
                                  <div className="property-featured">
                                    <a className="content-thumb" href="/">
                                      <img
                                       src={require('../../images/featured-properties2.jpg')}
                                        className="attachment-property-image" alt="" /> </a>
                                    <span className="property-category"><a href="/"
                                        typeof="skos:Concept" property="rdfs:label skos:prefLabel" datatype="">Single
                                        Family Home</a></span>
                                  </div>
                                  <div className="property-wrap">
                                    <h2 className="property-title"><a
                                        href="/"
                                        title="Single Family Residential, NJ">Single Family Residential, NJ</a></h2>
                                    <div className="property-excerpt">
                                      <p>classNameic 60's ranch living. House has hardwood floors and hard coat plaster
                                        walls and ceilings in good condition. Intimate backyard for private gatherings.
                                        Full basement leaves plenty of room for...</p>
                                    </div>
                                    <div className="property-summary">
                                      <div className="property-detail">
                                        <div className="size"><span>1,118 sqft</span>
                                        </div>
                                        <div className="bathrooms"><span>2</span>
                                        </div>
                                        <div className="bedrooms"><span>3</span>
                                        </div>
                                      </div>
                                      <div className="property-info">
                                        <div className="property-price">
                                          <span>
                                            <span className="amount">
                                              $ 299,000 </span>
                                          </span>
                                        </div>
                                        <div className="property-action">
                                          <a href="/">More Details <i
                                              className="fa fa-arrow-circle-o-right"></i></a>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </article>
                              </li>
                              <li>
                                <article className="hentry has-featured">
                                  <div className="property-featured">
                                    <a className="content-thumb" href="/">
                                      <img
                                        src={require('../../images/featured-properties3.jpg')}
                                        className="attachment-property-image" alt="" /> </a>
                                    <span className="property-category"><a href="/"
                                        typeof="skos:Concept" property="rdfs:label skos:prefLabel"
                                        datatype="">Condo</a></span>
                                  </div>
                                  <div className="property-wrap">
                                    <h2 className="property-title"><a href="/"
                                        title="Peter Cooper Village">Peter Cooper Village</a></h2>
                                    <div className="property-excerpt">
                                      <p>Peter Cooper Village/ Stuyvesant Town provides an unbeatable combination of
                                        city energy and community tranquility, providing insulation from the city that
                                        surrounds it, yet situated next to Manhattan's most dynamic...</p>
                                    </div>
                                    <div className="property-summary">
                                      <div className="property-detail">
                                        <div className="size"><span>1,304 sqft</span>
                                        </div>
                                        <div className="bathrooms"><span>2</span>
                                        </div>
                                        <div className="bedrooms"><span>3</span>
                                        </div>
                                      </div>
                                      <div className="property-info">
                                        <div className="property-price">
                                          <span>
                                            <span className="amount">
                                              $ 5,109 </span>
                                          </span>
                                        </div>
                                        <div className="property-action">
                                          <a href="/citilights/property/peter-cooper-village-0">More Details <i
                                              className="fa fa-arrow-circle-o-right"></i></a>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </article>
                              </li>
                              <li>
                                <article className="hentry has-featured">
                                  <div className="property-featured">
                                    <a className="content-thumb" href="/">
                                      <img
                                        src={require('../../images/featured-properties2.jpg')}
                                        className="attachment-property-image" alt="" /> </a>
                                    <span className="property-category"><a href="/citilights/taxonomy/term/12"
                                        typeof="skos:Concept" property="rdfs:label skos:prefLabel"
                                        datatype="">Co-op</a></span>
                                  </div>
                                  <div className="property-wrap">
                                    <h2 className="property-title"><a href="/"
                                        title="Villa Ernesto Subdivision">Villa Ernesto Subdivision</a></h2>
                                    <div className="property-excerpt">
                                      <p>Suspendisse blandit ligula turpis, ac convallis risus fermentum non. Duis
                                        vestibulum quis quam vel accumsan. Asunt in anim uis aute irure dolor in
                                        reprehenderit...</p>
                                    </div>
                                    <div className="property-summary">
                                      <div className="property-detail">
                                        <div className="size"><span>500 sqft</span>
                                        </div>
                                        <div className="bathrooms"><span>3</span>
                                        </div>
                                        <div className="bedrooms"><span>4</span>
                                        </div>
                                      </div>
                                      <div className="property-info">
                                        <div className="property-price">
                                          <span>
                                            <span className="amount">
                                              $ 1,000,000 </span>
                                          </span>
                                        </div>
                                        <div className="property-action">
                                          <a href="/">More Details <i
                                              className="fa fa-arrow-circle-o-right"></i></a>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </article>
                              </li>
                            </ul>
                          </div>
                          <a className="caroufredsel-prev" href="#"></a>
                          <a className="caroufredsel-next" href="#"></a>
                        </div>
                      </div>
                    </div>
                  </section>


                </div>
        
        </>
    );
}

export default Feature;