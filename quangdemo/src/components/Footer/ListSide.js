import React, {useState} from 'react'

import partner1 from '../../images/partner1.png';
import partner2 from '../../images/partner2.png';
import partner3 from '../../images/partner3.png';
import partner4 from '../../images/partner4.png';
import partner5 from '../../images/partner5.png';
import partner6 from '../../images/partner6.png';
const ListSide = (props) => {
    const [logo, setLogo] = useState(props.image)
    return(
        <>

<div className="panel-separator"></div>

<div className="panel-pane pane-block pane-views-testimonials-block">

  <section id="testimonial" className="wrap testimonial">
    <div className="container">
      <div className="testimonial-inner">
        <div className="section-title">
          <h3>Testimonial</h3>
        </div>
        <div className="testimonial-content">
          <div className="carousel slide" id="carousel-testimonial">
            <ol className="carousel-indicators">
              <li data-target="#carousel-testimonial" data-slide-to="0" className="active"></li>
              <li data-target="#carousel-testimonial" data-slide-to="1"></li>
              <li data-target="#carousel-testimonial" data-slide-to="2"></li>
              <li data-target="#carousel-testimonial" data-slide-to="3"></li>
              <li data-target="#carousel-testimonial" data-slide-to="4"></li>
              <li data-target="#carousel-testimonial" data-slide-to="5"></li>
            </ol>
            <div className="carousel-inner">
              <div className="item active">
                <div className="slide-content">
                  <div className="testimonial-desc">&ldquo;I found my current apartment on Citilights with
                    extraordinary help from them and totally satisfied with the choice I made. All I had
                    to do was to tell what I was looking for and I got back property suggestions nearly
                    exact to my imagination. Among those, I finally chose mine now then completed
                    procedure at ease. Highly recommend Citilights for your home search.&rdquo;</div>
                  <div className="our-customer-info">
                    <div className="avatar">
                    <a href="#" target="_blank">
                                    <img  class="item-image" src={require('../../images/customer1.png')} />
                                </a>
                    </div>
                    <div className="custom-desc">
                      <h4>Dave Softel</h4>
                      <p>Happy Buyer of June</p>
                    </div>
                  </div>
                </div>
              </div>
              <div className="item ">
                <div className="slide-content">
                  <div className="testimonial-desc">&ldquo;I found my current apartment on Citilights with
                    extraordinary help from them and totally satisfied with the choice I made. All I had
                    to do was to tell what I was looking for and I got back property suggestions nearly
                    exact to my imagination. Among those, I finally chose mine now then completed
                    procedure at ease. Highly recommend Citilights for your home search.&rdquo;</div>
                  <div className="our-customer-info">
                    <div className="avatar">
                    <a href="#" target="_blank">
                                    <img  class="item-image" src={require('../../images/customer1.png')}  />
                                </a>
                    </div>
                    <div className="custom-desc">
                      <h4>Dave Softel</h4>
                      <p>Happy Buyer of June</p>
                    </div>
                  </div>
                </div>
              </div>
              <div className="item ">
                <div className="slide-content">
                  <div className="testimonial-desc">&ldquo;I found my current apartment on Citilights with
                    extraordinary help from them and totally satisfied with the choice I made. All I had
                    to do was to tell what I was looking for and I got back property suggestions nearly
                    exact to my imagination. Among those, I finally chose mine now then completed
                    procedure at ease. Highly recommend Citilights for your home search.&rdquo;</div>
                  <div className="our-customer-info">
                    <div className="avatar">
                    <a href="#" target="_blank">
                                    <img  class="item-image" src={require('../../images/customer1.png')}  />
                                </a>
                    </div>
                    <div className="custom-desc">
                      <h4>Dave Softel</h4>
                      <p>Happy Buyer of June</p>
                    </div>
                  </div>
                </div>
              </div>
              <div className="item ">
                <div className="slide-content">
                  <div className="testimonial-desc">&ldquo;I found my current apartment on Citilights with
                    extraordinary help from them and totally satisfied with the choice I made. All I had
                    to do was to tell what I was looking for and I got back property suggestions nearly
                    exact to my imagination. Among those, I finally chose mine now then completed
                    procedure at ease. Highly recommend Citilights for your home search.&rdquo;</div>
                  <div className="our-customer-info">
                    <div className="avatar">
                    <a href="#" target="_blank">
                                    <img  class="item-image" src={require('../../images/customer1.png')}  />
                                </a>
                    </div>
                    <div className="custom-desc">
                      <h4>Dave Softel</h4>
                      <p>Happy Buyer of June</p>
                    </div>
                  </div>
                </div>
              </div>
              <div className="item ">
                <div className="slide-content">
                  <div className="testimonial-desc">&ldquo;I found my current apartment on Citilights with
                    extraordinary help from them and totally satisfied with the choice I made. All I had
                    to do was to tell what I was looking for and I got back property suggestions nearly
                    exact to my imagination. Among those, I finally chose mine now then completed
                    procedure at ease. Highly recommend Citilights for your home search.&rdquo;</div>
                  <div className="our-customer-info">
                    <div className="avatar">
                    <a href="#" target="_blank">
                                    <img  class="item-image" src={require('../../images/customer1.png')}  />
                                </a>
                    </div>
                    <div className="custom-desc">
                      <h4>Dave Softel</h4>
                      <p>Happy Buyer of June</p>
                    </div>
                  </div>
                </div>
              </div>
              <div className="item ">
                <div className="slide-content">
                  <div className="testimonial-desc">&ldquo;I found my current apartment on Citilights with
                    extraordinary help from them and totally satisfied with the choice I made. All I had
                    to do was to tell what I was looking for and I got back property suggestions nearly
                    exact to my imagination. Among those, I finally chose mine now then completed
                    procedure at ease. Highly recommend Citilights for your home search.&rdquo;</div>
                  <div className="our-customer-info">
                    <div className="avatar">
                    <a href="#" target="_blank">
                                    <img  class="item-image" src={require('../../images/customer1.png')}  />
                                </a>
                     
                    </div>
                    <div className="custom-desc">
                      <h4>Dave Softel</h4>
                      <p>Happy Buyer of June</p>
                    </div>
                  </div>
                </div>
              </div>

            </div>
          </div>
        </div>
      </div>
    </div>
  </section>


</div>
        </>
    );
}


export default ListSide