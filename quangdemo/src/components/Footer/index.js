import React from 'react'
import ListSide from './ListSide'
import partner1 from '../../images/partner1.png';
import partner2 from '../../images/partner2.png';
import partner3 from '../../images/partner3.png';
import partner4 from '../../images/partner4.png';
import partner5 from '../../images/partner5.png';
import partner6 from '../../images/partner6.png';
const Footer = () => {
    return(
        <>
            <ListSide />
            <div className="panel-separator"></div>
                <div className="panel-pane pane-block pane-views-partners-block">

                  <section id="our-partners" className="wrap our-partners">
                    <div className="container">
                      <div className="our-partners-inner">
                        <div className="row">
                            <div class="col-sm-6 col-md-2 item-col">
                            <a href="#" target="_blank">
                                    <img  class="item-image" src={partner1} />
                                </a>
                            </div>
                            <div class="col-sm-6 col-md-2 item-col">
                            <a href="#" target="_blank">
                                    <img  class="item-image" src={partner2} />
                                </a>
                            </div>
                            <div class="col-sm-6 col-md-2 item-col">
                            <a href="#" target="_blank">
                                    <img  class="item-image" src={partner3} />
                                </a>
                            </div>
                            <div class="col-sm-6 col-md-2 item-col">
                            <a href="#" target="_blank">
                                    <img  class="item-image" src={partner4} />
                                </a>
                            </div>
                            <div class="col-sm-6 col-md-2 item-col">
                            <a href="#" target="_blank">
                                    <img  class="item-image" src={partner5} />
                                </a>
                            </div>
                            <div class="col-sm-6 col-md-2 item-col">
                            <a href="#" target="_blank">
                                    <img  class="item-image" src={partner6} />
                                </a>
                            </div>
                        </div>
                      </div>
                    </div>
                  </section>

            </div>
        </>
    );
}


export default Footer