import React from 'react'
import '../../sass/main.scss'
const Background = require('../../images/search-box-bg.jpg')
const Search = () => {

    return (
        <div class="panel-pane pane-block pane-noo-citilights-properties-search-default"  >
        <section className="wrap search-box" style={{
            backgroundImage: `url(${Background})`,
            backgroundPosition: 'center',
            backgroundSize: 'cover',
            backgroundRepeat: 'no-repeat'
        }}>
            <div className="gsearch">
                <div className="container">

                    <div className="gsearch-info">
                        <h4 className="gsearch-info-title">Find your place</h4>
                        <div className="gsearch-info-content">Instantly find your desired place from your own idea of location, at any price 
                             and other elements just by starting your search now
                        </div>
                        </div>


                        <form className="gsearchform"  id="citilights-map-filter-form" accept-charset="UTF-8">
                            <div>
                            <div className="gsearch-wrap">
                                <div className="gsearchform">
                                    <div className="gsearch-content">
                                        <div className="gsearch-field">
                                            <div className="form-group glocation">
                                                <div className="label-select">
                                                    <div className="form-item form-type-select form-item-glocation">
                                                        <select className="form-control form-select" id="edit-glocation" name="glocation"><option value="All">All Locations</option><option value="5">California</option><option value="2">New York</option><option value="4">Utah</option></select>
                                                    </div>
                                                </div>
                                            </div>

                                            <div className="form-group gsub-location">
                                                <div className="label-select">
                                                    <div id="wrapper-map-filter-sub-location"><div className="form-item form-type-select form-item-gsub-location">
                                                        <select className="form-control form-select" id="edit-gsub-location" name="gsub-location"><option value="All">All Sub-locations</option></select>
                                                    </div>
                                                    </div>          </div>
                                            </div>

                                            <div className="form-group gstatus">
                                                <div className="label-select">
                                                    <div className="form-item form-type-select form-item-gstatus">
                                                        <select className="form-control form-select" id="edit-gstatus" name="gstatus"><option value="All">All Status</option><option value="7">For rent</option><option value="10">Open House</option><option value="8">Pending</option><option value="9">Sale</option><option value="6">Sold</option></select>
                                                    </div>
                                                </div>
                                            </div>

                                            <div className="form-group gtype">
                                                <div className="label-select">
                                                    <div className="form-item form-type-select form-item-gtype">
                                                        <select className="form-control form-select" id="edit-gtype" name="gtype"><option value="All">All Type</option><option value="11">Apartment</option><option value="12">Co-op</option><option value="13">Condo</option><option value="14">Single Family Home</option></select>
                                                    </div>
                                                </div>
                                            </div>

                                            <div className="form-group gbed">
                                                <div className="label-select">
                                                    <div className="form-item form-type-select form-item-gbed">
                                                        <select className="form-control form-select" id="edit-gbed" name="gbed"><option value="All">No. of Bedrooms</option><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option><option value="6">6</option></select>
                                                    </div>
                                                </div>
                                            </div>

                                            <div className="form-group gbath">
                                                <div className="label-select">
                                                    <div className="form-item form-type-select form-item-gbath">
                                                        <select className="form-control form-select" id="edit-gbath" name="gbath"><option value="All">No. of Bathrooms</option><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option></select>
                                                    </div>
                                                </div>
                                            </div>

                                            <div className="form-group gprice">
                                                <span className="gprice-label">Price</span>
                                                <div className="gslider-range gprice-slider-range"></div>
                                                <span className="gslider-range-value gprice-slider-range-value-min"></span>
                                                <span className="gslider-range-value gprice-slider-range-value-max"></span>
                                                <div className="gprice-input js-hide">
                                                    <div className="form-item form-type-textfield form-item-gprice-min">
                                                        <input type="text" id="edit-gprice-min" name="gprice-min" value="" size="60" maxlength="128" className="form-text" />
                                                    </div>
                                                    <div className="form-item form-type-textfield form-item-gprice-max">
                                                        <input type="text" id="edit-gprice-max" name="gprice-max" value="" size="60" maxlength="128" className="form-text" />
                                                    </div>
                                                </div>
                                            </div>

                                            <div className="form-group garea">
                                                <span className="garea-label">Area</span>
                                                <div className="gslider-range garea-slider-range"></div>
                                                <span className="gslider-range-value garea-slider-range-value-min"></span>
                                                <span className="gslider-range-value garea-slider-range-value-max"></span>
                                                <div className="garea-input js-hide">
                                                    <div className="form-item form-type-textfield form-item-garea-min">
                                                        <input type="text" id="edit-garea-min" name="garea-min" value="" size="60" maxlength="128" className="form-text" />
                                                    </div>
                                                    <div className="form-item form-type-textfield form-item-garea-max">
                                                        <input type="text" id="edit-garea-max" name="garea-max" value="" size="60" maxlength="128" className="form-text" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div className="gsearch-action">
                                            <div className="gsubmit">
                                                <a className="btn btn-deault gsubmit-button-action" >Search Property</a>
                                                <div className="js-hide gsubmit-button"><div className="form-actions form-wrapper" id="edit-actions"><input type="submit" id="edit-submit" name="op"  className="form-submit" /></div></div>
                                            </div>
                                        </div>
                                    </div>
                                    <input type="hidden" name="form_build_id" value="form-ZY3KemW8qd2TKSYshp_363lM4tUKqKUzpFOnh21iJvg" />
                                    <input type="hidden" name="form_id" value="citilights_map_filter_form" />

                                </div>
                            </div>


                        </div>
                        </form>      
                    </div>
                </div>
  </section>
  </div>
    );

}

export default Search;
