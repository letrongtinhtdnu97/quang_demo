import React from 'react'


import Search from '../components/Search'
import Table from '../components/Table'
import Feature from '../components/Feature'
import List from '../components/List'
import Footer from '../components/Footer'
const Home = () => {

    return(
        <div>
            <Search />
            <Table />
            <Feature />
            <List />
            <Footer />
        </div>
    );

}

export default Home;
